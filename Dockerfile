FROM ruby:2.3.0

RUN apt-get update && \
    apt-get install -y nodejs postgresql-client --no-install-recommends && \
    rm -rf /var/lib/apt/lists/* && \
    mkdir -p /usr/src/app

# RUN bundle config --global frozen 1

WORKDIR /usr/src/app
